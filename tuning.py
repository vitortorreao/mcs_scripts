"""Read the results from the java code and outputs the following:
   - The Friedman test p-value;
   - The Nemenyi post-test if necessary;
   - The average ranks of each competitor;
   - The LaTeX table, ready for being put in the document."""

import csv
import sys
import Orange
import matplotlib.pyplot as plt
import numpy as np

from scipy.stats import friedmanchisquare

from nemenyi import NemenyiTestPostHoc

COMPETITORS = ["default", "0.5", "0.6", "0.7", "0.8", "0.9"]
DATASET_MAP = {
    1: "autos",
    2: "balance-scale",
    3: "breast-cancer",
    4: "colic",
    5: "dermatology",
    6: "ecoli",
    7: "flags",
    8: "glass",
    9: "haberman",
    10: "heart-c",
    11: "heart-h",
    12: "heart-statlog",
    13: "hepatitis",
    14: "ionosphere",
    15: "liver-disorders",
    16: "lymph",
    17: "primary-tumor",
    18: "sonar",
    19: "vote",
    20: "wine"
}

def max_candidate(candidates, dataset, results):
    """Retrieves the best candidate"""
    max_mean = (-1, -1)
    max_cand = ""
    for candidate in candidates:
        if results[dataset][candidate][0] > max_mean[0]:
            max_mean = results[dataset][candidate]
            max_cand = candidate
        elif results[dataset][candidate][0] == max_mean[0]:
            if results[dataset][candidate][1] > max_mean[1]:
                max_mean = results[dataset][candidate]
                max_cand = candidate
    return max_cand

class Candidate(object):
    """A candidate for ranking"""

    def __init__(self, dataset, name, mean, std):
        self.dataset = dataset
        self.name = name
        self.mean = mean
        self.std = std

    def __gt__(self, other):
        return self.mean > other.mean or \
               (self.mean == other.mean and self.std < other.std)

    def __lt__(self, other):
        return not self.__ge__(other)

    def __eq__(self, other):
        return self.mean == other.mean and self.std == other.std

    def __ne__(self, other):
        return not self.__eq__(other)

    def __le__(self, other):
        return self.__eq__(other) or self.__lt__(other)

    def __ge__(self, other):
        return self.__eq__(other) or self.__gt__(other)

    def __repr__(self):
        return "{0}: {1} +/- {2}".format(self.name, self.mean, self.std)

def main(competitors):
    """Main function"""
    results = dict()
    with open(sys.argv[1], "r") as csv_file:
        reader = csv.DictReader(csv_file, delimiter=";")
        for data in reader:
            dataset = data['file name'].split('.')[0]
            results[dataset] = dict()
            for candidate in competitors:
                results[dataset][candidate] = (
                    float(data[candidate + ' (mean)']),
                    float(data[candidate + ' (std)'])
                )
    positions = dict()
    for dataset in results:
        positions[dataset] = dict()
        candidates = [Candidate(dataset, candidate,
                                results[dataset][candidate][0],
                                results[dataset][candidate][1])
                      for candidate in results[dataset]]
        candidates = sorted(candidates, reverse=True)
        curr_pos = 1
        positions[dataset][candidates[0].name] = curr_pos
        for i in range(1, len(candidates)):
            if candidates[i] != candidates[i - 1]:
                curr_pos += 1
            positions[dataset][candidates[i].name] = curr_pos
    avg_ranks = dict()
    for candidate in competitors:
        avg_ranks[candidate] = 0.0
        num_datasets = 0
        for dataset in results:
            avg_ranks[candidate] += positions[dataset][candidate]
            num_datasets += 1
        avg_ranks[candidate] /= num_datasets
    cand_measures = list()
    for candidate in competitors:
        cand_measures.append([results[d][candidate][0] for d in results])
    _, pvalue = friedmanchisquare(*cand_measures)
    print("Friedman test pvalue = {0}".format(pvalue))
    print("Average Ranks = " + str(avg_ranks))
    print("LaTeX -----------------------------------------")
    print("  \\hline")
    print("  " + " & ".join(["Id"] + competitors) + " \\\\")
    print("  \\hline")
    for code in DATASET_MAP:
        line = "  {0}".format(code)
        for candidate in competitors:
            if positions[DATASET_MAP[code]][candidate] > 1:
                line += " & ${0:04.1f}\\pm{1:04.1f}$".format(
                    results[DATASET_MAP[code]][candidate][0] * 100,
                    results[DATASET_MAP[code]][candidate][1] * 100
                )
            else:
                line += " & \\underline{{${0:04.1f}\\pm{1:04.1f}$}}".format(
                    results[DATASET_MAP[code]][candidate][0] * 100,
                    results[DATASET_MAP[code]][candidate][1] * 100
                )
        line += " \\\\"
        print(line)
    print("  \\hline")
    rank_line = "    "
    for candidate in competitors:
        rank_line += " & ${0:04.2f}$".format(avg_ranks[candidate])
    rank_line += " \\\\"
    print(rank_line)
    print("End of LaTeX ------------------------------------------------------")
    if pvalue < 0.05: # use post-test if friedman's null hypothesis is rejected
        # we need to invert the measures, because the nemenyi code ranks from
        # lowest to highest
        measures = list()
        for measure in cand_measures:
            measures.append([1 - val for val in measure])
        nemenyi = NemenyiTestPostHoc(np.asarray(measures))
        mean_ranks, _ = nemenyi.do()
        print(mean_ranks)
        cdiff = 3.16368342 * ((10 * 11 / (6.0 * 20)) ** 0.5)
        print("Nemenyi critical difference = {0}".format(cdiff))
        Orange.evaluation.graph_ranks(mean_ranks, competitors, cd=cdiff)
        plt.show()

if __name__ == "__main__":
    main(COMPETITORS)
