"""Read the results from the java code and outputs the following:
   - The Friedman test p-value;
   - The Nemenyi post-test if necessary;
   - The average ranks of each competitor;
   - The LaTeX table, ready for being put in the document."""

from tuning import main

COMPETITORS = ["MLKNN", "CLR", "OLA", "LCA", "DV", "DS", "DVS", "KNORAE", "MCB",
               "MV"]

if __name__ == "__main__":
    main(COMPETITORS)
